# Re-assign user content

## Table of contents

- Introduction
- Installation
- Maintainers
- Requirements
- configuration

 ## Introduction

 The Re-assign User Content module allows you to reassign content
 of user you are about to delete to another user.

 ## Installation

Install as you would normally install a contributed Drupal module. For further
  information, see
  [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

 ## Requirements

  This module need no requirements.

 ## Configuration

  The module has no menu or modifiable settings. There is no configuration.
  When enabled, the module will add new option in account cancel page
  to assign the content of deleted user to another user.

 ## Maintainers

 Creator:
 - Berramou - https://www.drupal.org/u/berramou
